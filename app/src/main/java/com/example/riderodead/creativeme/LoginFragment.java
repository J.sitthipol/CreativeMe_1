package com.example.riderodead.creativeme;

import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.Fragment;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.riderodead.creativeme.databinding.FragmentLoginBinding;

import static android.content.ContentValues.TAG;

/**
 * Created by RIDERODEAD on 07-Feb-18.
 */

public class LoginFragment extends Fragment {

    FragmentLoginBinding mBinding;
    String hearder = "";


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        initInstance();
        return mBinding.getRoot();
    }

    private void initInstance() {
        final ProfileFragment profileFragment = new ProfileFragment();
        mBinding.linearLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

            }
        });


        mBinding.btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: " + mBinding.etLogin.getText() + " : " + mBinding.etPassword.getText());
                if (mBinding.etLogin.getText().toString().equals("root") && mBinding.etPassword.getText().toString().equals("1234")) {

                    getFragmentManager().beginTransaction()
                            .replace(R.id.contentContainer, profileFragment)
                            .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_in_left)
                            .commit();
                } else if (mBinding.etLogin.getText().toString().equals("root") != true && mBinding.etLogin.getText().toString().equals("") != true) {
                    hearder = "Username is wrong";
                    dialogAlert(hearder);

                } else if (mBinding.etPassword.getText().toString().equals("1234") != true && mBinding.etPassword.getText().toString().equals("") != true) {
                    hearder = "Password is wrong";
                    dialogAlert(hearder);

                } else if (mBinding.etLogin.getText().toString().equals("") && mBinding.etPassword.getText().toString().equals("")) {
                    hearder = "Please enter Username and Password";
                    dialogAlert(hearder);
                } else {
                    hearder = "Username and Password are wrong";
                    dialogAlert(hearder);

                }


            }
        });


    }

    public void dialogAlert(String header) {
        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());
        builder.setMessage(header);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Toast.makeText(getActivity(),
                        "Hint : root ", Toast.LENGTH_SHORT).show();
            }
        });

        builder.show();
    }


}
