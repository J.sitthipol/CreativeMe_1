package com.example.riderodead.creativeme;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.riderodead.creativeme.databinding.FragmentProfileBinding;

/**
 * Created by RIDERODEAD on 07-Feb-18.
 */

public class ProfileFragment extends Fragment {

    FragmentProfileBinding mBinding;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile,container,false);
        initInstance();
        return mBinding.getRoot();
    }

    private void initInstance() {
        final LoginFragment loginFragment = new LoginFragment();

        mBinding.tvName.setText("Sitthipol");
        mBinding.tvSurname.setText("Janyawarangkul");
        mBinding.tvTel.setText("0806164646");
        mBinding.tvEmail.setText("jan.sitthiol@gmail.com");

        mBinding.btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction()
                        .replace(R.id.contentContainer, loginFragment)
                        .commit();
            }
        });
    }
}
